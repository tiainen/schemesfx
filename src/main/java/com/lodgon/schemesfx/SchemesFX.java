package com.lodgon.schemesfx;

import com.lodgon.schemesfx.dalicloud.DaliCloudService;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SchemesFX extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Schemes");

        DaliCloudService service = new DaliCloudService();

        Scene scene = new Scene(new Dashboard(service), 1200, 700);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
