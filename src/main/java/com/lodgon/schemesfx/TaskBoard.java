package com.lodgon.schemesfx;

import com.lodgon.dalicloud.client.entity.User;
import com.lodgon.schemesfx.control.SchemeListCell;
import com.lodgon.schemesfx.control.TaskListCell;
import com.lodgon.schemesfx.dalicloud.DaliCloudService;
import com.lodgon.schemesfx.dalicloud.Scheme;
import com.lodgon.schemesfx.dalicloud.Task;
import com.lodgon.schemesfx.dalicloud.TaskDefinition;
import java.util.Calendar;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Joeri
 */
public class TaskBoard extends BorderPane {

    private final ObservableList<Scheme> schemes = FXCollections.observableArrayList();
    private final ObservableList<Task> tasks = FXCollections.observableArrayList();

    public TaskBoard(DaliCloudService service) {
        User user = service.getUserClient().getByEmail("joeri@sertik.net");
        schemes.addAll(service.getUserSchemes(user));

        ComboBox<Scheme> cmSchemes = new ComboBox<>(schemes);
        cmSchemes.setButtonCell(new SchemeListCell());
        cmSchemes.setCellFactory(v -> new SchemeListCell());
        cmSchemes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            List<TaskDefinition> taskDefinitions = service.getTaskDefinitions(newValue);
            for (TaskDefinition taskDefinition : taskDefinitions) {
                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);
                now.set(Calendar.MILLISECOND, 0);
                Calendar then = Calendar.getInstance();
                switch (taskDefinition.getPeriodType()) {
                    case DAY:
                        now.add(Calendar.DAY_OF_MONTH, -1 * (taskDefinition.getPeriodCount() - 1));
                        then.setTimeInMillis(now.getTimeInMillis());
                        then.add(Calendar.DAY_OF_MONTH, taskDefinition.getPeriodCount());
                        break;
                    case WEEK:
                        int daysToGoToBeginningOfWeek = 0;
                        if (now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                            daysToGoToBeginningOfWeek = 6;
                        } else {
                            daysToGoToBeginningOfWeek = now.get(Calendar.DAY_OF_WEEK) - 2;
                        }
                        now.add(Calendar.DAY_OF_MONTH, -1 * (daysToGoToBeginningOfWeek + (taskDefinition.getPeriodCount() - 1) * 7));
                        then.setTimeInMillis(now.getTimeInMillis());
                        then.add(Calendar.DAY_OF_MONTH, taskDefinition.getPeriodCount() * 7);
                        break;
                    case MONTH:
                        now.set(Calendar.DAY_OF_MONTH, 1);
                        now.add(Calendar.MONTH, -1 * (taskDefinition.getPeriodCount() - 1));
                        then.setTimeInMillis(now.getTimeInMillis());
                        then.add(Calendar.MONTH, taskDefinition.getPeriodCount());
                        break;
                    case YEAR:
                        now.set(Calendar.MONTH, Calendar.JANUARY);
                        now.set(Calendar.DAY_OF_MONTH, 1);
                        now.add(Calendar.YEAR, -1 * (taskDefinition.getPeriodCount() - 1));
                        then.setTimeInMillis(now.getTimeInMillis());
                        then.add(Calendar.YEAR, taskDefinition.getPeriodCount());
                        break;
                }
                then.add(Calendar.MILLISECOND, -1);

                Task task = new Task();
                task.setId(java.util.UUID.randomUUID().toString());
                task.setState(Task.State.PENDING);
                task.setCreationDate(System.currentTimeMillis());
                task.setTaskDefinitionId(taskDefinition.getId());
                task.setTaskDefinition(taskDefinition);
                task.setDateStart(now.getTimeInMillis());
                task.setDateEnd(then.getTimeInMillis());
                tasks.add(task);
            }
        });

        setTop(cmSchemes);

        ListView<Task> lvTasks = new ListView<>(tasks);
        lvTasks.setCellFactory(v -> new TaskListCell());
        setCenter(lvTasks);

        for (Scheme scheme : schemes) {
            if (scheme.isCurrent()) {
                cmSchemes.getSelectionModel().select(scheme);
                break;
            }
        }
    }

}
