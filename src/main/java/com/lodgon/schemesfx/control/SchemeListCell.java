package com.lodgon.schemesfx.control;

import com.lodgon.schemesfx.dalicloud.Scheme;
import javafx.scene.control.ListCell;

/**
 *
 * @author Joeri
 */
public class SchemeListCell extends ListCell<Scheme> {

    @Override
    protected void updateItem(Scheme item, boolean empty) {
         super.updateItem(item, empty);

         if (item != null && !empty) {
             if (item.isCurrent()) {
                setText(item.getTitle() + " [C]");
             } else {
                setText(item.getTitle());
             }
         } else {
             setText(null);
             setGraphic(null);
         }
    }

}
