package com.lodgon.schemesfx.control;

import com.lodgon.schemesfx.dalicloud.Task;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import javafx.scene.control.ListCell;

/**
 *
 * @author Joeri
 */
public class TaskListCell extends ListCell<Task> {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", new Locale("NL_be"));
    static {
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
    }

    @Override
    protected void updateItem(Task item, boolean empty) {
         super.updateItem(item, empty);

         if (item != null && !empty) {
             StringBuilder sb = new StringBuilder();
             sb.append(item.getTaskDefinition().getTitle()).append(": ")
                     .append(item.getState()).append(".")
                     .append(" Start date: ").append(sdf.format(item.getDateStart()))
                     .append(", End date: ").append(sdf.format(item.getDateEnd()));
             setText(sb.toString());
         } else {
             setText(null);
             setGraphic(null);
         }
    }

}
