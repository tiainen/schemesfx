package com.lodgon.schemesfx.control;

import com.lodgon.schemesfx.dalicloud.TaskDefinition;
import javafx.scene.control.ListCell;

/**
 *
 * @author Joeri
 */
public class TaskDefinitionListCell extends ListCell<TaskDefinition> {

    @Override
    protected void updateItem(TaskDefinition item, boolean empty) {
         super.updateItem(item, empty);

         if (item != null && !empty) {
             StringBuilder sb = new StringBuilder();
             sb.append(item.getTitle()).append(": ").append(item.getPeriodType().getPeriodString(item.getPeriodCount()));
             setText(sb.toString());
         } else {
             setText(null);
             setGraphic(null);
         }
    }

}
