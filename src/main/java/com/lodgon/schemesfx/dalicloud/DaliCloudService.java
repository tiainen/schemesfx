package com.lodgon.schemesfx.dalicloud;

import com.lodgon.dalicloud.client.ContentClient;
import com.lodgon.dalicloud.client.UserClient;
import com.lodgon.dalicloud.client.entity.SearchResult;
import com.lodgon.dalicloud.client.entity.User;
import java.util.List;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author joeri
 */
public class DaliCloudService {

    private final UserClient<User> userClient;
    private final ContentClient<Scheme> schemeClient;
    private final ContentClient<Task> taskClient;
    private final ContentClient<TaskDefinition> taskDefinitionClient;

    public static final String DALICLOUD_LOCATION = "http://b.dalicloud.com";
    public static final String DALICLOUD_PUBLIC_KEY = "795b6e74827545ff49603004";
    public static final String DALICLOUD_SECRET_KEY = "ead6ac62d070cb28ef96e0305e482fcb9b5e765eeb919bdf";

    public DaliCloudService() {
        userClient = new UserClient<>(User.class, new GenericType<SearchResult<User>>() {});
        userClient.setLocation(DALICLOUD_LOCATION);
        userClient.setPublicKey(DALICLOUD_PUBLIC_KEY);
        userClient.setSecretKey(DALICLOUD_SECRET_KEY);

        schemeClient = new ContentClient<>(Scheme.class, new GenericType<SearchResult<Scheme>>() {});
        schemeClient.setLocation(DALICLOUD_LOCATION);
        schemeClient.setPublicKey(DALICLOUD_PUBLIC_KEY);
        schemeClient.setSecretKey(DALICLOUD_SECRET_KEY);

        taskClient = new ContentClient<>(Task.class, new GenericType<SearchResult<Task>>() {});
        taskClient.setLocation(DALICLOUD_LOCATION);
        taskClient.setPublicKey(DALICLOUD_PUBLIC_KEY);
        taskClient.setSecretKey(DALICLOUD_SECRET_KEY);

        taskDefinitionClient = new ContentClient<>(TaskDefinition.class, new GenericType<SearchResult<TaskDefinition>>() {});
        taskDefinitionClient.setLocation(DALICLOUD_LOCATION);
        taskDefinitionClient.setPublicKey(DALICLOUD_PUBLIC_KEY);
        taskDefinitionClient.setSecretKey(DALICLOUD_SECRET_KEY);
    }

    public UserClient<User> getUserClient() {
        return userClient;
    }

    public ContentClient<Scheme> getSchemeClient() {
        return schemeClient;
    }

    public ContentClient<Task> getTaskClient() {
        return taskClient;
    }

    public ContentClient<TaskDefinition> getTaskDefinitionClient() {
        return taskDefinitionClient;
    }

    public List<Scheme> getUserSchemes(User user) {
        return schemeClient.listByAuthor(user.getId(), 0, 100);
    }

    public List<TaskDefinition> getTaskDefinitions(Scheme scheme) {
        return taskDefinitionClient.listByParent(scheme.getId(), null, 0, 100);
    }
}
