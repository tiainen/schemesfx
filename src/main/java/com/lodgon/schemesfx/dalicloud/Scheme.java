package com.lodgon.schemesfx.dalicloud;

import com.lodgon.dalicloud.client.entity.Content;

/**
 *
 * @author joeri
 */
public class Scheme extends Content {

    private boolean current;

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

}
