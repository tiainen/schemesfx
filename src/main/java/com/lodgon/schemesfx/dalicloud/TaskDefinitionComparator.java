package com.lodgon.schemesfx.dalicloud;

import java.util.Comparator;

/**
 *
 * @author Joeri
 */
public class TaskDefinitionComparator implements Comparator<TaskDefinition> {

    @Override
    public int compare(TaskDefinition t1, TaskDefinition t2) {
        if (t1.getPeriodType() != null && t2.getPeriodType() != null) {
            Integer t1PeriodTotal = calculatePeriodTotal(t1);
            Integer t2PeriodTotal = calculatePeriodTotal(t2);
            if (!t1PeriodTotal.equals(t2PeriodTotal)) {
                return t1PeriodTotal.compareTo(t2PeriodTotal);
            }
        }

        if (t1.getTitle() != null && t2.getTitle() != null) {
            return t1.getTitle().compareTo(t2.getTitle());
        } else {
            return t1.getId().compareTo(t2.getId());
        }
    }

    private int calculatePeriodTotal(TaskDefinition taskDefinition) {
        switch (taskDefinition.getPeriodType()) {
            case DAY:
                return taskDefinition.getPeriodCount();
            case WEEK:
                return 7 * taskDefinition.getPeriodCount();
            case MONTH:
                return 30 * taskDefinition.getPeriodCount();
            case YEAR:
                return 365 * taskDefinition.getPeriodCount();
        }
        return Integer.MAX_VALUE;
    }
    
}
