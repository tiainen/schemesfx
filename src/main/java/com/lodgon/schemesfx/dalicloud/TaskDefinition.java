package com.lodgon.schemesfx.dalicloud;

import com.lodgon.dalicloud.client.entity.Content;

/**
 *
 * @author joeri
 */
public class TaskDefinition extends Content {

    private Integer periodCount;
    private PeriodType periodType;
    private Integer durationCount;
    private DurationType durationType;

    public Integer getPeriodCount() {
        return periodCount;
    }

    public void setPeriodCount(Integer periodCount) {
        this.periodCount = periodCount;
    }

    public PeriodType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(PeriodType periodType) {
        this.periodType = periodType;
    }

    public Integer getDurationCount() {
        return durationCount;
    }

    public void setDurationCount(Integer durationCount) {
        this.durationCount = durationCount;
    }

    public DurationType getDurationType() {
        return durationType;
    }

    public void setDurationType(DurationType durationType) {
        this.durationType = durationType;
    }

    public enum PeriodType {
        DAY,
        WEEK,
        MONTH,
        YEAR;

        public String getPeriodString(int count) {
            if (count == 0) {
                return "never";
            } else if (count == 1) {
                return "every " + name().toLowerCase();
            } else {
                return "every " + count + " " + name().toLowerCase() + "s";
            }
        }
    }

    public enum DurationType {
        MINUTE,
        HOUR
    }
}
