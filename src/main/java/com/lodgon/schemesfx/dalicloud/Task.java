package com.lodgon.schemesfx.dalicloud;

import com.lodgon.dalicloud.client.entity.Content;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author joeri
 */
public class Task extends Content {

    private Long dateStart;
    private Long dateEnd;
    private Long dateStarted;
    private Long dateCompleted;
    private State state;
    private String taskDefinitionId;
    private TaskDefinition taskDefinition;

    public Long getDateStart() {
        return dateStart;
    }

    public void setDateStart(Long dateStart) {
        this.dateStart = dateStart;
    }

    public Long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Long getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Long dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Long getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Long dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getTaskDefinitionId() {
        return taskDefinitionId;
    }

    public void setTaskDefinitionId(String taskDefinitionId) {
        this.taskDefinitionId = taskDefinitionId;
    }

    @XmlTransient
    public TaskDefinition getTaskDefinition() {
        return taskDefinition;
    }

    public void setTaskDefinition(TaskDefinition taskDefinition) {
        this.taskDefinition = taskDefinition;
    }

    public enum State {
        PENDING,
        ACTIVE,
        COMPLETE,
        OVERDUE,
        PAUSED,
        CANCELED;
    }

}
