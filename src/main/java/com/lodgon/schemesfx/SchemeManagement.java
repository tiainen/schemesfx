package com.lodgon.schemesfx;

import com.lodgon.dalicloud.client.entity.User;
import com.lodgon.schemesfx.control.SchemeListCell;
import com.lodgon.schemesfx.control.TaskDefinitionListCell;
import com.lodgon.schemesfx.dalicloud.DaliCloudService;
import com.lodgon.schemesfx.dalicloud.Scheme;
import com.lodgon.schemesfx.dalicloud.TaskDefinition;
import com.lodgon.schemesfx.dalicloud.TaskDefinitionComparator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

/**
 *
 * @author joeri
 */
public class SchemeManagement extends BorderPane {

    private final ObservableList<Scheme> schemes = FXCollections.observableArrayList();
    private final ObservableList<TaskDefinition> taskDefinitions = FXCollections.observableArrayList();
    private final ObjectProperty<Scheme> selectedScheme = new SimpleObjectProperty();

    public SchemeManagement(DaliCloudService service) {
        User user = service.getUserClient().getByEmail("joeri@sertik.net");
        schemes.addAll(service.getUserSchemes(user));

        ListView<Scheme> lvSchemes = new ListView<>(schemes);
        lvSchemes.setCellFactory(v -> new SchemeListCell());
        lvSchemes.setMinWidth(300.0);

        Button addScheme = new Button("new");
        addScheme.setOnAction(e -> {
            Optional<String> text = Dialogs.create()
                    .masthead("Enter a name for the new scheme:")
                    .title("New scheme")
                    .showTextInput();
            if (text.isPresent() && !text.get().isEmpty()) {
                Scheme scheme = new Scheme();
                scheme.setAuthorId(user.getId());
                scheme.setTitle(text.get());
                scheme.setCurrent(false);
                scheme = service.getSchemeClient().create(scheme);
                schemes.add(scheme);
            }
        });

        Button editScheme = new Button("edit");
        editScheme.disableProperty().bind(lvSchemes.getSelectionModel().selectedIndexProperty().lessThan(0));
        editScheme.setOnAction(e -> {
            Optional<String> text = Dialogs.create()
                    .masthead("Enter a name for the scheme:")
                    .title("Edit scheme")
                    .showTextInput(lvSchemes.getSelectionModel().getSelectedItem().getTitle());
            if (text.isPresent()) {
                Scheme scheme = lvSchemes.getSelectionModel().getSelectedItem();
                scheme.setTitle(text.get());
                scheme = service.getSchemeClient().update(scheme);
                schemes.set(lvSchemes.getSelectionModel().getSelectedIndex(), scheme);
            }
        });

        Button deleteScheme = new Button("delete");
        deleteScheme.disableProperty().bind(lvSchemes.getSelectionModel().selectedIndexProperty().lessThan(0));
        deleteScheme.setOnAction(e -> {
            Scheme scheme = lvSchemes.getSelectionModel().getSelectedItem();
            Action action = Dialogs.create()
                    .masthead(null)
                    .message("Are you sure you want to delete the scheme " + scheme.getTitle() + " together with all its task definitions?")
                    .title("Delete scheme")
                    .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                    .showConfirm();
            if (action.equals(Dialog.Actions.YES)) {
                if (service.getSchemeClient().delete(scheme.getId(), true)) {
                    schemes.remove(scheme);
                }
            }
        });

        Button loadScheme = new Button("load");
        loadScheme.disableProperty().bind(lvSchemes.getSelectionModel().selectedIndexProperty().lessThan(0));
        loadScheme.setOnAction(e -> {
            selectedScheme.set(lvSchemes.getSelectionModel().getSelectedItem());
            taskDefinitions.clear();
            Set<TaskDefinition> sortedSchemeTaskDefinitions = new TreeSet<>(new TaskDefinitionComparator());
            sortedSchemeTaskDefinitions.addAll(service.getTaskDefinitions(selectedScheme.get()));
            taskDefinitions.addAll(sortedSchemeTaskDefinitions);
        });

        Button setCurrentScheme = new Button("current");
        setCurrentScheme.disableProperty().bind(lvSchemes.getSelectionModel().selectedIndexProperty().lessThan(0));
        setCurrentScheme.setOnAction(e -> {
            Scheme scheme = lvSchemes.getSelectionModel().getSelectedItem();
            if (!scheme.isCurrent()) {
                schemes.stream()
                        .filter(s -> s.isCurrent())
                        .findFirst().ifPresent(currentScheme -> {
                            currentScheme.setCurrent(false);
                            Scheme updatedScheme = service.getSchemeClient().update(currentScheme);
                            schemes.set(lvSchemes.getItems().indexOf(currentScheme), updatedScheme);
                        });

                scheme.setCurrent(true);
                scheme = service.getSchemeClient().update(scheme);
                schemes.set(lvSchemes.getSelectionModel().getSelectedIndex(), scheme);
            }
        });

        HBox schemeButtons = new HBox(10.0);
        schemeButtons.getChildren().addAll(addScheme, editScheme, deleteScheme, loadScheme, setCurrentScheme);

        Label schemesLabel = new Label("Schemes");
        schemesLabel.setFont(Font.font(16.0));

        ListView<TaskDefinition> lvTaskDefinitions = new ListView<>(taskDefinitions);
        lvTaskDefinitions.setCellFactory(v -> new TaskDefinitionListCell());

        Button addTaskDefinition = new Button("new");
        addTaskDefinition.disableProperty().bind(selectedScheme.isNull());
        addTaskDefinition.setOnAction(e -> {
            TaskDefinitionForm form = new TaskDefinitionForm();
            Dialog dialog = new Dialog(this, "New task definition");
            dialog.getActions().addAll(Dialog.Actions.OK, Dialog.Actions.CANCEL);
            dialog.setContent(form);
            Action action = dialog.show();
            if (action.equals(Dialog.Actions.OK)) {
                Optional<TaskDefinition> optTaskDefinition = form.getTaskDefinition();
                if (optTaskDefinition.isPresent()) {
                    TaskDefinition taskDefinition = optTaskDefinition.get();
                    taskDefinition.setParentId(lvSchemes.getSelectionModel().getSelectedItem().getId());
                    taskDefinition = service.getTaskDefinitionClient().create(taskDefinition);
                    taskDefinitions.add(taskDefinition);
                }
            }
        });
        Button editTaskDefinition = new Button("edit");
        editTaskDefinition.disableProperty().bind(lvTaskDefinitions.getSelectionModel().selectedIndexProperty().lessThan(0));
        editTaskDefinition.setOnAction(e -> {
            TaskDefinitionForm form = new TaskDefinitionForm(lvTaskDefinitions.getSelectionModel().getSelectedItem());
            Dialog dialog = new Dialog(this, "Edit task definition");
            dialog.getActions().addAll(Dialog.Actions.OK, Dialog.Actions.CANCEL);
            dialog.setContent(form);
            Action action = dialog.show();
            if (action.equals(Dialog.Actions.OK)) {
                Optional<TaskDefinition> optTaskDefinition = form.getTaskDefinition();
                if (optTaskDefinition.isPresent()) {
                    TaskDefinition taskDefinition = optTaskDefinition.get();
                    taskDefinition = service.getTaskDefinitionClient().update(taskDefinition);
                    taskDefinitions.set(lvTaskDefinitions.getSelectionModel().getSelectedIndex(), taskDefinition);
                }
            }
        });
        Button deleteTaskDefinition = new Button("delete");
        deleteTaskDefinition.disableProperty().bind(lvTaskDefinitions.getSelectionModel().selectedIndexProperty().lessThan(0));
        deleteTaskDefinition.setOnAction(e -> {
            TaskDefinition taskDefinition = lvTaskDefinitions.getSelectionModel().getSelectedItem();
            Action action = Dialogs.create()
                    .masthead(null)
                    .message("Are you sure you want to delete the task definition " + taskDefinition.getTitle() + "?")
                    .title("Delete task definition")
                    .actions(Dialog.Actions.YES, Dialog.Actions.NO)
                    .showConfirm();
            if (action.equals(Dialog.Actions.YES)) {
                if (service.getTaskDefinitionClient().delete(taskDefinition.getId(), true)) {
                    taskDefinitions.remove(taskDefinition);
                }
            }
        });

        HBox taskDefinitionButtons = new HBox(10.0);
        taskDefinitionButtons.getChildren().addAll(addTaskDefinition, editTaskDefinition, deleteTaskDefinition);

        Label taskDefinitionsLabel = new Label("Task Definitions");
        taskDefinitionsLabel.setFont(Font.font(16.0));

        VBox left = new VBox(10.0);
        VBox.setVgrow(lvSchemes, Priority.ALWAYS);
        left.setPadding(new Insets(5.0));
        left.getChildren().addAll(schemesLabel, schemeButtons, lvSchemes);

        VBox center = new VBox(10.0);
        VBox.setVgrow(lvTaskDefinitions, Priority.ALWAYS);
        center.setPadding(new Insets(5.0, 5.0, 5.0, 0.0));
        center.getChildren().addAll(taskDefinitionsLabel, taskDefinitionButtons, lvTaskDefinitions);

        setLeft(left);
        setCenter(center);
    }

    private class TaskDefinitionForm extends GridPane {

        private final ValidationSupport validationSupport = new ValidationSupport();

        private TaskDefinition taskDefinition;

        private final TextField title = new TextField();
        private final ComboBox<TaskDefinition.PeriodType> periodType;
        private final ComboBox<Integer> periodCount;
        private final ComboBox<TaskDefinition.DurationType> durationType;
        private final ComboBox<String> durationCount;

        public TaskDefinitionForm() {
            this(new TaskDefinition());
        }

        public TaskDefinitionForm(TaskDefinition taskDefinition) {
            setHgap(10.0);
            setVgap(10.0);

            this.taskDefinition = taskDefinition;

            title.setText(taskDefinition.getTitle());

            this.add(new Label("Title"), 0, 0);
            this.add(title, 1, 0);
            GridPane.setHgrow(title, Priority.ALWAYS);
            validationSupport.registerValidator(title, Validator.createEmptyValidator("Title is required"));

            ObservableList<Integer> periodCounts = FXCollections.observableArrayList(1, 2, 3, 4, 5, 6);
            ObservableList<TaskDefinition.PeriodType> periodTypes = FXCollections.observableArrayList(TaskDefinition.PeriodType.values());
            periodType = new ComboBox(periodTypes);
            periodType.getSelectionModel().select(taskDefinition.getPeriodType());
            periodCount = new ComboBox(periodCounts);
            periodCount.getSelectionModel().select(taskDefinition.getPeriodCount());

            HBox period = new HBox(5.0);
            period.getChildren().addAll(periodType, periodCount);
            this.add(new Label("Period"), 0, 1);
            this.add(period, 1, 1);
            GridPane.setHgrow(periodType, Priority.ALWAYS);
            validationSupport.registerValidator(periodType, Validator.createEmptyValidator("Period type is required"));
            validationSupport.registerValidator(periodCount, Validator.createEmptyValidator("Period count is required"));

            ObservableList<String> durationCounts = FXCollections.observableArrayList("1", "2", "3", "4", "5", "10", "15", "20", "25", "30", "45", "60", "90");
            ObservableList<TaskDefinition.DurationType> durationTypes = FXCollections.observableArrayList(TaskDefinition.DurationType.values());
            durationType = new ComboBox(durationTypes);
            durationType.getSelectionModel().select(taskDefinition.getDurationType());
            durationCount = new ComboBox(durationCounts);
            durationCount.setEditable(true);
            if (taskDefinition.getDurationCount() != null) {
                durationCount.setValue(String.valueOf(taskDefinition.getDurationCount()));
            }

            HBox duration = new HBox(5.0);
            duration.getChildren().addAll(durationType, durationCount);
            this.add(new Label("Duration"), 0, 2);
            this.add(duration, 1, 2);
            GridPane.setHgrow(durationType, Priority.ALWAYS);
            validationSupport.registerValidator(durationType, Validator.createEmptyValidator("Duration type is required"));
            validationSupport.registerValidator(durationCount, (Control c, String newValue) -> {
                Integer newIntegerValue = 0;
                try {
                    newIntegerValue = Integer.parseInt(newValue);
                } catch (NumberFormatException ex) {
                    // ignore
                }
                return ValidationResult.fromErrorIf(durationCount, "Duration type must be an integer > 0", newIntegerValue <= 0);
            });

            ColumnConstraints column1 = new ColumnConstraints();
            column1.setPercentWidth(30.0);
            ColumnConstraints column2 = new ColumnConstraints();
            column2.setPercentWidth(70.0);

            getColumnConstraints().addAll(column1, column2);
        }

        public Optional<TaskDefinition> getTaskDefinition() {
            if (validationSupport.isInvalid()) {
                return Optional.empty();
            } else {
                taskDefinition.setTitle(title.getText());
                taskDefinition.setPeriodCount(periodCount.getSelectionModel().getSelectedItem());
                taskDefinition.setPeriodType(periodType.getSelectionModel().getSelectedItem());
                taskDefinition.setDurationCount(Integer.parseInt(durationCount.getSelectionModel().getSelectedItem()));
                taskDefinition.setDurationType(durationType.getSelectionModel().getSelectedItem());
                return Optional.of(taskDefinition);
            }
        }
    }
}
