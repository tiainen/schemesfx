package com.lodgon.schemesfx;

import com.lodgon.schemesfx.dalicloud.DaliCloudService;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author Joeri
 */
public class Dashboard extends BorderPane {

    public Dashboard(DaliCloudService service) {
        SchemeManagement schemeManagement = new SchemeManagement(service);
        TaskBoard taskBoard = new TaskBoard(service);

        ToggleGroup toggleGroup = new ToggleGroup();

        ToggleButton manageSchemes = new ToggleButton("schemes");
        manageSchemes.setToggleGroup(toggleGroup);
        manageSchemes.setOnAction(e -> {
           setCenter(schemeManagement); 
        });
        ToggleButton manageTasks = new ToggleButton("tasks");
        manageTasks.setToggleGroup(toggleGroup);
        manageTasks.setOnAction(e -> {
            setCenter(taskBoard);
        });

        HBox top = new HBox(10.0);
        top.getChildren().addAll(manageSchemes, manageTasks);
        setTop(top);

        toggleGroup.selectToggle(manageTasks);
        setCenter(taskBoard);
    }
}
